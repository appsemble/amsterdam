import { URL } from 'url';

import axios from 'axios';
import { Argv } from 'yargs';

export const command = 'import-containers';
export const description = 'Import bread app containers and locations from one API into another';

interface Args {
  downloadRemote: string;
  downloadId: number;
  uploadRemote: string;
  uploadId: number;
}

interface Container {
  id: number;
  externalId: number;
  latitude: string;
  longitude: string;
}

interface Location {
  id: number;
  containers: number[];
}

export function build(argv: Argv): Argv {
  return argv
    .option('download-remote', {
      desc: 'The remote to download data from',
      default: 'https://appsemble.app',
    })
    .option('download-id', {
      type: 'number',
      desc: 'The app id to download data from',
      default: 28,
    })
    .option('upload-remote', {
      desc: 'The remote to upload data into',
      default: 'http://localhost:9999',
    })
    .option('upload-id', {
      type: 'number',
      desc: 'The app id to upload data into',
      demandOption: true,
    });
}

export async function handler({
  downloadId,
  downloadRemote,
  uploadId,
  uploadRemote,
}: Args): Promise<void> {
  /**
   * The host to upload data to, i.e. http://localhost:9999.
   */
  const uploader = axios.create({ baseURL: String(new URL(`/api/${uploadId}`, uploadRemote)) });

  /**
   * The host to download data from, i.e. http://appsemble.app
   */
  const downloader = axios.create({
    baseURL: String(new URL(`/api/${downloadId}`, downloadRemote)),
  });

  let { data: localContainers } = await downloader.get<Container[]>('breadContainer');
  let { data: localLocations } = await downloader.get<Location[]>('location');
  const { data: externalContainers } = await uploader.get<Container[]>('resources/breadContainer');

  if (localContainers.length === 0) {
    await Promise.all(
      externalContainers.map((ec) =>
        uploader.post('breadContainer', {
          ...ec,
          longitude: String(ec.longitude),
          latitude: String(ec.latitude),
        }),
      ),
    );

    ({ data: localContainers } = await uploader.get('breadContainer'));
  }

  if (localLocations.length === 0) {
    const { data: externalLocations } = await downloader.get<Location[]>('resources/location');
    await Promise.all(externalLocations.map((el) => uploader.post('location', el)));

    ({ data: localLocations } = await uploader.get('location'));
  }

  const newLocations = localLocations.map((location) => ({
    ...location,
    containers: location.containers.map((container) => {
      const extCon = externalContainers.find((caa) => caa.id === container).externalId;
      const locCon = localContainers.find((lcc) => lcc.externalId === extCon);

      return locCon.id;
    }),
  }));

  const newContainers = localContainers.map((container) => ({
    ...container,
    locationId: newLocations.find((nl) => nl.containers.includes(container.id)).id,
    longitude: String(container.longitude),
    latitude: String(container.latitude),
  }));

  await Promise.all([
    ...newContainers.map((container) => uploader.put(`breadContainer/${container.id}`, container)),
    ...newLocations.map((location) => uploader.put(`location/${location.id}`, location)),
  ]);
}
