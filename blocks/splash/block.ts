export {};

declare module '@appsemble/sdk' {
  interface Actions {
    /**
     * This action is dispatched with the given data.
     */
    onLoad: never;

    /**
     * This action is dispatched if the load action has failed.
     */
    onError: never;

    /**
     * This action is dispatched if the load action was successful.
     */
    onSuccess: never;
  }
}
