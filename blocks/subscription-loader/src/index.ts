import { bootstrap } from '@appsemble/sdk';

import { ResourceSubscription } from '../block';

bootstrap(({ actions, events, parameters: { field = 'id' } }) => {
  async function processData(d: ResourceSubscription): Promise<void> {
    const subs = Object.entries(d?.subscriptions || {})
      .filter(([, value]) => value.create || value.update)
      .map(([key]) => key);

    const query = {
      $filter:
        subs.length === 0 ? `${field} eq -1` : `${field} eq ${subs.join(` or ${field} eq `)}`,
    };

    const result = await actions.onLoad(query);
    events.emit.data(result);
  }

  events.on.subscriptions(processData);
});
